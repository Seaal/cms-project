<?php

require_once("Validator.php");
require_once("ValidatorResult.php");
require_once("PageDAO.php");
require_once("PageNotFoundException.php");

class PageValidator extends Validator
{
  public function __construct()
  {
  }

  public function validate($formData)
  {
    $result = new ValidatorResult();

    $result->addResult("module", $this->required($formData["module"]));
    $result->addResult("title", $this->validateTitle($formData["title"]));
    $result->addResult("ref", $this->validateRef($formData["ref"], $formData["module"], $formData["oldRef"]));
    $result->addResult("hidden", $this->required($formData["hidden"]));

    return $result;
  }

  private function validateTitle(&$title)
  {
    $result = $this->required($title);

    if($result === true)
    {
      return $this->maxLength($title,40);
    }
    else
    {
      return $result;
    }
  }

  private function validURL($data)
  {
    if(preg_match("/^[a-zA-Z0-9_-]+$/", $data))
    {
      return true;
    }
    else
    {
      return "Please only enter URL friendly characters. ( Letters, Numbers, _ and - )";
    }
  }

  private function noDoubleSeparator($data)
  {
    if(strpos($data, '__') === false && strpos($data, '--') === false &&
       strpos($data, '-_') === false && strpos($data, '_-') === false)
    {
      return true;
    }
    else
    {
      return "Please do not put one separator ( _ or - ) after another.";
    }
  }

  private function startWithChar($data)
  {
    if(preg_match("/^[^_-]/", $data))
    {
      return true;
    }
    else
    {
      return "Please do not start with a - or _";
    }
  }

  private function validateRef(&$ref, $moduleID, &$oldRef)
  {
    $result = $this->required($ref);

    if($result === true)
    {
      $result = $this->maxLength($ref,40);

      if($result === true)
      {
        $pdao = new PageDAO();

        $result = $this->validURl($ref);

        if($result === true)
        {
          $result = $this->startWithChar($ref);

          if($result === true)
          {
            $result = $this->noDoubleSeparator($ref);

            if($result === true)
            {
              try
              {
                $page = $pdao->getPageByRef($ref, true, $moduleID);

                if(isset($oldRef) && $oldRef == $page->reference)
                {
                  $result = true;
                }
                else
                {
                  $result = "This page reference already exists.";
                }
              }
              catch(PageNotFoundException $e)
              {
                $result = true;
              }
            }
          }
        }
      }
    }
    return $result;
  }
}
?>