<?php

require_once("Dropdown.php");
require_once("ModuleDAO.php");

function getYearDropdown($year)
{
  $mdao = new ModuleDAO();

  

  $modules = $mdao->getCodesSemestersByYear($year);

  if(empty($modules))
  {
    return "";
  }
  else
  {
    $label = "Year ".$year;
    $dd = new Dropdown($label);

    $semesters = $mdao->getSemesters();

    $first = true;

    foreach($semesters as $semester)
    {
        if(!empty($modules[$semester->semester_id]))
        {
          if(!$first)
          {
            $dd->addSeparator();
          }
          else
          {
            $first = false;
          }

          $dd->addLabel($semester->name);

          foreach($modules[$semester->semester_id] as $code)
          {
            $dd->addLink($code,"#");
          }
      }
    }

    return $dd->getHTML();
  }
}

?>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <?php
              echo getYearDropdown(1);
              echo getYearDropdown(2);
              echo getYearDropdown(3);
              echo getYearDropdown(4);
            ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </div>