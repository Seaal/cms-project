<?php

require_once("PageDAO.php");
require_once("Page.php");

$title = "Index";
$pageOptions = getPageNavbarLinks();
$body = "";

if(isset($_GET['page']))
{
  $fact = new PageDAO();
  $page = $fact->getPageByRef($_GET['page'], true);
  $title = $page->title;
  $body = $page;
}

?>

<div class="col-sm-3 col-md-2 sidebar">
  <ul class="nav nav-sidebar">
    <?php getPageNavbarLinks(); ?>
    <li class="active"><a href="#">Overview</a></li>
    <li><a href="#">Reports</a></li>
    <li><a href="#">Analytics</a></li>
    <li><a href="#">Export</a></li>
  </ul>
</div>