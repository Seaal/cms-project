$(function(){

  $.validator.setDefaults({
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function (error, element) {
        if (element.attr('type') == "radio") {
          error.insertAfter(element.closest("div").find(".last-radio"));
        } else {
          error.insertAfter(element);
        }
      }
  });
})