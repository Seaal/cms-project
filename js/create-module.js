$(function(){
  $("#createModuleForm").validate({
    rules: {
      name: {
        required: true,
        maxlength: 100
      },
      code: {
        remote: "/cms-project/ajax/checkModuleCodeUnique.php?year="+year,
        required: true,
        maxlength: 15,
        alphanumericChars: true
      },
      schoolYear: {
        required: true
      },
      semester: {
        required: true
      }
    },
    messages: {
      code: {
        remote: "This module code already exists."
      }
    }
  });
});