var request;
var previewButtonHTML;
var pageID;
var pageRef;
var moduleCode;
var moduleID;
var year;

function initPage(pageid, previewButton, code, moduleid, moduleYear, pageref)
{
  pageID = pageid;
  previewButtonHTML = previewButton;
  moduleCode = code;
  moduleID = moduleid;
  year = moduleYear;
  pageRef = pageref;
}

$(function(){

  $('#editPageForm').hide();

  $('#editPageForm').find('#inputTitle').bind('input', function() { 
    var input = $(this).val();
    input = input.toLowerCase();
    input = input.replace(/ /g, '-');
    input = input.replace(/[^A-z0-9-]/g, '');
    input = input.replace(/+-/g, '-');
    input = input.replace(/\^/g, '');
    $('#editPageForm').find('#inputRef').val(input);
    $('#editPageForm').valid();
  });

  function getEmptySectionMessage() {
    return "<p>[Empty Section] (Click to Edit)</p>";
  }

  function saveSection(event){
    var $div = $(this).parent();

    if($div.find('.editable').html() != getEmptySectionMessage())
    {

      request = $.ajax({
        url: "ajax/saveSection.php",
        type: "post",
        data: { section : $div.attr("id"),
        data : $div.find('.editable').html() }
      });

      request.done(function (response, textStatus, jqXHR){
        if(response) {
          $div.append(response);
          $div.find(".alert-md").fadeOut(3000);
          savedHTML = getSavedHTML();
        }
      });

      request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
          "The following error occured: "+
          textStatus, errorThrown
          );
      });

      request.always(function () {});
   }
  };

  function deleteSection(event){
    if($('.section').length > 1) {
      if(confirm("Are you sure you want to delete this section?")) {
        //Abort any pending request
        if (request) {
          request.abort();
        }

        var $div = $(this).parent();

        request = $.ajax({
          url: "ajax/deleteSection.php",
          type: "post",
          data: { section : $div.attr("id") }
        });

        request.done(function (response, textStatus, jqXHR){
          if(response == 'true') {
            $div.remove();
          }
          else
          {
            $div.append(response);
          }

          savedHTML = getSavedHTML();
        });

        request.fail(function (jqXHR, textStatus, errorThrown){
          console.error(
            "The following error occured: "+
            textStatus, errorThrown
            );
        });

        request.always(function () {});
      }
    }
  };

  function showSection(event){
    setSectionVisibility(event, 0, $(this).parent());
  }

  function hideSection(event){
    setSectionVisibility(event, 1, $(this).parent());
  }

  function setSectionVisibility(event, hidden, $div){
    //Abort any pending request
    if (request) {
      request.abort();
    }

    request = $.ajax({
      url: "ajax/setSectionVisibility.php",
      type: "post",
      data: { section : $div.attr("id"),
      hidden : hidden }
    });

    request.done(function (response, textStatus, jqXHR){
      $div.find('.visibility-btn').replaceWith(response);
      $div.find('.hideSection').click(hideSection);
      $div.find('.showSection').click(showSection);

      if(hidden) {
        $div.find('.editable').addClass("section-hidden");
      }
      else {
        $div.find('.editable').removeClass("section-hidden");
      }
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
      console.error(
        "The following error occured: "+
        textStatus, errorThrown
        );
    });

    request.always(function () {});
  };

  function moveSectionUp(event)
  {
    $div = $(this).parent();
    if(!$div.is('.section:first'))
    {
      swapSectionOrder(event, $div.prev(), $div);
    }
  }

  function moveSectionDown(event)
  {
    $div = $(this).parent();
    if(!$div.is('.section:last'))
    {
      swapSectionOrder(event, $div, $div.next());
    }
  }

  function swapSectionOrder(event, $firstSection, $secondSection)
  {
    //Abort any pending request
    if (request) {
      request.abort();
    }

    request = $.ajax({
      url: "ajax/swapSectionOrder.php",
      type: "post",
      data: { firstSection : $firstSection.attr("id"),
      secondSection : $secondSection.attr("id") }
    });

    request.done(function (response, textStatus, jqXHR){
      if(response == 'true')
      {
        $secondSection.after($firstSection);

        savedHTML = getSavedHTML();
      }
      else
      {
        $('#errrorMessage').html(response);
      }
      
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
      console.error(
        "The following error occured: "+
        textStatus, errorThrown
        );
    });

    request.always(function () {});
  }

  //Add Button Click Listeners
  $('.saveSection').click(saveSection);
  $('.deleteSection').click(deleteSection);
  $('.showSection').click(showSection);
  $('.hideSection').click(hideSection);
  $('.moveSectionUp').click(moveSectionUp);
  $('.moveSectionDown').click(moveSectionDown);

  $('#createNewSection').click(function(event){
    //Abort any pending request
    if (request) {
      request.abort();
    }

    request = $.ajax({
      url: "ajax/createSection.php",
      type: "post",
      data: { page : pageID }
    });

    request.done(function (response, textStatus, jqXHR){
      $('#sections').append(response);
      $('#sections .section:last > .deleteSection').click(deleteSection);
      $('#sections .section:last > .saveSection').click(saveSection);
      $('#sections .section:last > .hideSection').click(hideSection);
      $('#sections .section:last > .moveSectionUp').click(moveSectionUp);
      $('#sections .section:last > .moveSectionDown').click(moveSectionDown);

      savedHTML += getEmptySectionMessage();
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
      console.error(
        "The following error occured: "+
        textStatus, errorThrown
        );
    });

    request.always(function () {});
  });

  $('#saveAllSections').click(function(){
    $('.saveSection').click();
  })

  function getEditButton() {
    return '<button type="button" class="btn btn-warning header-btn pull-right" id="editPage">Edit <span class="glyphicon glyphicon-pencil"></span></button>';
  }

  function getPreviewButton() {
    return previewButtonHTML;
  }

  function getPreviewDiv() {
    return '<div id="preview"></div>';
  }

  var $content;
  var previewHTML;
  var previewFullHTML;
  var preview = false;

  function editPage() {
    $('#preview').remove();
    $('#page').append($content);
    $('#editPage').replaceWith(getPreviewButton());
    $('#previewPage').click(previewPage);

    preview = false;
  }

  function previewPage() {
    previewFullHTML = getSavedHTML();
    previewHTML = '';

    $('.editable').each(function() {
      if(!($(this).html() == getEmptySectionMessage()) &&
         !($(this).hasClass("section-hidden")))
      {
        previewHTML += $(this).html();
      }
    })

    $content = $('#content').detach();

    $('#page').append(getPreviewDiv());
    $('#preview').append(previewHTML);

    $('#previewPage').replaceWith(getEditButton());
    $('#editPage').click(editPage);

    
    preview = true;
  }

  function deletePage(){
    if(confirm("Are you sure you want to delete this Page?"))
    {
      //Abort any pending request
      if (request) {
        request.abort();
      }

      request = $.ajax({
        url: "ajax/deletePage.php",
        type: "post",
        data: { page : pageID }
      });

      request.done(function (response, textStatus, jqXHR){
        if(response) {
          window.location.href = "moduleOverview.php?code=" + moduleCode + "&year=" + year;
        }
      });

      request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
          "The following error occured: "+
          textStatus, errorThrown
          );
      });

      request.always(function () {});
    }
  };

  $('#editPageForm').submit(function(event){
    if($(this).valid())
    {
      //Abort any pending request
      if (request) {
        request.abort();
      }

      var $form = $(this);
      var $inputs = $form.find("input, select, button, textarea");
      var serializedData = $form.serialize();
      serializedData += "&page=" + pageID + "&module=" + moduleID + "&oldRef=" + pageRef;

      //Disable the form until the ajax is finished
      $inputs.prop("disabled", true);

      request = $.ajax({
        url: "ajax/editPage.php",
        type: "post",
        data: serializedData
      });

      request.done(function (response, textStatus, jqXHR){
        if($.trim(response)[0] != "<")
        {
          window.location.href = "viewPage.php?code=" + moduleCode + "&ref=" + response + "&year=" + year;
        }
        else
        {
          $('#errorMessage').html(response);
        }
      });

      request.fail(function (jqXHR, textStatus, errorThrown){
        // log the error to the console
        console.error(
          "The following error occured: "+
          textStatus, errorThrown
        );
      });

      request.always(function () {
          //Reenable the form
          $inputs.prop("disabled", false);
      });
    }

    event.preventDefault();
  });

  $('#previewPage').click(previewPage);
  $('#deletePage').click(deletePage);

  $('#editPage').click(function(){
    $('#editPageForm').slideToggle();
  });

  $("#editPageForm").validate({
    rules: {
      title: {
        required: true,
        maxlength: 40
      },
      ref: {
        remote: "ajax/checkPageRefUnique.php?module="+moduleID+"&oldRef="+pageRef, 
        required: true,
        maxlength: 40,
        urlChars: true,
        startWithChar: true,
        noDoubleSeparator: true
        }
    },
    messages: {
      ref: {
        remote: "This page reference already exists."
      }
    }
  });

  function getSavedHTML()
  {
    var html;

    $('.editable').each(function() {
        html += $.trim($(this).html());
    })

    return html;
  }

  var savedHTML = getSavedHTML();

  $(window).bind('beforeunload', function(e){
    if(preview) {
      if(savedHTML != previewFullHTML) {
        return true;
      }
      else {
        e = null;
      }
    }
    else if(savedHTML != getSavedHTML()) {
      return true;
    }
    else {
      e = null;
    }
  });
});