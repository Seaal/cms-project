var moduleID;
var request;

function initModuleID(moduleid) {
  moduleID = moduleid;
}

$(function(){

  function deleteModule(){
    if(confirm("Are you sure you want to delete this Module?"))
    {
      //Abort any pending request
      if (request) {
        request.abort();
      }

      request = $.ajax({
        url: "ajax/deleteModule.php",
        type: "post",
        data: { module : moduleID }
      });

      request.done(function (response, textStatus, jqXHR){
        if(response == 'true') {
          window.location.href = "index.php";
        }
        else {
          $('#errorMessage').html(response);
        }
      });

      request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
          "The following error occured: "+
          textStatus, errorThrown
          );
      });

      request.always(function () {});
    }
  };

  $('#deleteModule').click(deleteModule);

  $("#editModuleForm").validate({
    rules: {
      name: {
        required: true,
        maxlength: 100
      },
      code: {
        required: true,
        maxlength: 15,
        alphanumericChars: true,
        remote: "/cms-project/ajax/checkModuleCodeUnique.php?year="+year+"&oldCode="+code
        }
    },
    messages: {
      code: {
        remote: "This module code already exists."
      }
    }
  });
});