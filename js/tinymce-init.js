function initTinyMce()
{
  tinymce.init({
    selector: "div.editable",
    inline: true,
    plugins: [
      "advlist autolink lists link image charmap anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste textcolor responsivefilemanager"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image",
    image_advtab: true,  
    external_filemanager_path:"/cms-project/filemanager/",
    filemanager_title:"Filemanager" ,
    external_plugins: { "filemanager" : "/cms-project/filemanager/plugin.min.js"}
  });
}