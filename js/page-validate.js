$(function(){
  var validUrlCharactersRegex = new RegExp(/^[a-zA-Z0-9_-]+$/);
  var doesNotStartWithDashOrUnderscore = new RegExp(/^[^_-]/);

  $.validator.addMethod(
    "urlChars",
    function (value, element) {
      return validUrlCharactersRegex.test(value);
    },
    "Please only enter URL friendly characters. ( Letters, Numbers, _ and - )"
  );

  $.validator.addMethod(
    "startWithChar",
    function (value, element) {
      return doesNotStartWithDashOrUnderscore.test(value)
    },
    "Please do not start with a - or _"
  );

  $.validator.addMethod(
    "noDoubleSeparator",
    function (value, element) {
      return value.indexOf('__') == -1 && value.indexOf('--') == -1 && value.indexOf('_-') == -1 && value.indexOf('-_') == -1;
    },
    "Please do not put one separator ( _ or - ) after another."
  );
});