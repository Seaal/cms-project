$(function(){
  $("#rollover").click(function(e)
  {
    if(confirm("Are you sure you want to rollover? This is not a reversable operation.")) {
      $("#rollover").attr("disabled", true);

      var request = $.ajax({
        url: "ajax/rollover.php",
        type: "post"
      });

      request.done(function (response, textStatus, jqXHR){
        window.location.href = "admin.php";
      });

      request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
          "The following error occured: "+
          textStatus, errorThrown
          );
      });

      request.always(function () {
        $("#rollover").attr("disabled", false);
      });
    }
  });
});