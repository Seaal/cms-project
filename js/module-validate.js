var code;
var year;

function initModuleValidation(modulecode, moduleyear)
{
  code = modulecode;
  year = moduleyear;
}

$(function(){

  var validAlphanumericRegex = new RegExp(/^[a-zA-Z0-9]+$/);

  $.validator.addMethod(
    "alphanumericChars",
    function (value, element) {
      return validAlphanumericRegex.test(value);
    },
    "Please only enter Alphanumeric Characters."
  );
}); 