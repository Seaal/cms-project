$(function() {

  $('#createPageForm [name="title"]').bind('input', function() { 
    var input = $('#createPageForm [name="title"]').val();
    input = input.toLowerCase();
    input = input.replace(/ /g, '-');
    input = input.replace(/[^A-z0-9-]/g, '');
    input = input.replace(/+-/g, '-');
    input = input.replace(/\^/g, '');
    $('#createPageForm [name="ref"]').val(input);
    $('#createPageForm').valid();
  });

  var request;

  function movePageUp(event)
  {
    event.preventDefault();

    $li = $(this).closest("li");
    if(!$li.is('.sidebar-link:nth-child(2)'))
    {
      swapPageOrder(event, $li.prev(), $li);
    }
  }

  function movePageDown(event)
  {
    event.preventDefault();

    $li = $(this).closest("li");
    if(!$li.is('.sidebar-link:last'))
    {
      swapPageOrder(event, $li, $li.next());
    }
  }

  function swapPageOrder(event, $firstPage, $secondPage)
  {
    //Abort any pending request
    if (request) {
      request.abort();
    }

    request = $.ajax({
      url: "ajax/swapPageOrder.php",
      type: "post",
      data: { firstPage : $firstPage.attr("data-page-id"),
      secondPage : $secondPage.attr("data-page-id") }
    });

    request.done(function (response, textStatus, jqXHR){
      $secondPage.after($firstPage);
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
      console.error(
        "The following error occured: "+
        textStatus, errorThrown
        );
    });

    request.always(function () {});
  }

  $('#createPageForm').submit(function(event){
    if($('#createPageForm').valid()){
      //Abort any pending request
      if (request) {
        request.abort();
      }

      var $form = $(this);
      var $inputs = $form.find("input, select, button, textarea");
      var serializedData = $form.serialize();

      //Disable the form until the ajax is finished
      $inputs.prop("disabled", true);

      request = $.ajax({
        url: "ajax/createPage.php",
        type: "post",
        data: serializedData
      });

      request.done(function (response, textStatus, jqXHR){
        $('#sidebarLinks').append(response);
        $('#sidebarLinks .sidebar-link:last > a > .movePageUp').click(movePageUp);
        $('#sidebarLinks .sidebar-link:last > a > .movePageDown').click(movePageDown);
        sidebarPageValidate.resetForm();
        $('#createPageForm [name=title]').val("");
        $('#createPageForm [name=ref]').val("");
      });

      request.fail(function (jqXHR, textStatus, errorThrown){
        // log the error to the console
        console.error(
          "The following error occured: "+
          textStatus, errorThrown
        );
      });

      request.always(function () {
          //Reenable the form
          $inputs.prop("disabled", false);
      });
    }

    event.preventDefault();
  });

  $('.movePageUp').click(movePageUp);
  $('.movePageDown').click(movePageDown);

  var sidebarPageValidate = $("#createPageForm").validate({
    rules: {
      title: {
        required: true,
        maxlength: 40
      },
      ref: {
        remote: "ajax/checkPageRefUnique.php?module="+moduleID,
        required: true,
        maxlength: 40,
        urlChars: true,
        startWithChar: true,
        noDoubleSeparator: true
      }
    },
    messages: {
      ref: {
        remote: "This page reference already exists."
      }
    }
  });
});