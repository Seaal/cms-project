$("#editPageForm").validate({
    rules: {
      title: {
        required: true,
        maxlength: 40
      },
      newRef: {
        required: true,
        maxlength: 40,
        urlChars: true,
        startWithChar: true,
        noDoubleSeparator: true
        }
    }
  });