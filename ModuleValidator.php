<?php

require_once("Validator.php");
require_once("ValidatorResult.php");
require_once("ModuleDAO.php");
require_once("ModuleNotFoundException.php");

class ModuleValidator extends Validator
{
  public function __construct()
  {
  }

  public function validate($formData)
  {
    $result = new ValidatorResult();

    $result->addResult("name", $this->validateName($formData["name"]));
    $result->addResult("code", $this->validateCode($formData["code"], $formData["year"], $formData["oldCode"]));
    $result->addResult("schoolYear", $this->required($formData["schoolYear"]));
    $result->addResult("semester", $this->required($formData["semester"]));
    $result->addResult("hidden", $this->required($formData["hidden"]));

    return $result;
  }

  private function validateName(&$name)
  {
    $result = $this->required($name);

    if($result === true)
    {
      return $this->maxLength($name,100);
    }
    else
    {
      return $result;
    }
  }

  private function alphaNumeric($data)
  {
    if(preg_match("/^[A-Za-z0-9]+$/", $data))
    {
      return true;
    }
    else
    {
      return "Please only enter Alphanumeric Characters.";
    }
  }

  private function validateCode(&$code, $year, &$oldCode)
  {
    $result = $this->required($code);

    if($result === true)
    {
      $result = $this->maxLength($code,100);

      if($result === true)
      {
        $result = $this->alphaNumeric($code);

        if($result === true)
        {

          $mdao = new ModuleDAO();

          try
          {
            $module = $mdao->getModuleByCode($code, true, $year);

            if(isset($oldCode) && $oldCode == $module->code)
            {
              $result = true;
            }
            else
            {
              $result = "This module code already exists.";
            }
          }
          catch(ModuleNotFoundException $e)
          {
            $result = true;
          }
        }
      }
    }

    return $result;
  }
}
?>