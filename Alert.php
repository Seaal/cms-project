<?php

class Alert
{
  private $type;
  private $message;

  public function __construct($message, $type)
  {
    $this->type = $type;
    $this->message = $message;
  }

  public function getHTML()
  {
    $HTML = "
    <div class=\"alert-md alert-".$this->type." alert-dismissable\">
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
      <strong>".$this->message."</strong>
    </div>";

    return $HTML;
  }

  public function __toString()
  {
    return $this->getHTML();
  }
}

?>