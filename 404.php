<?php
require_once("UserDAO.php");
require_once("ModuleDAO.php");

require_once("TopBar.php");

session_start();

$loggedIn = false;

if(isset($_SESSION["userID"]))
{
  $udao = new UserDAO();
  $user = $udao->getUserByID($_SESSION["userID"]);
  if($user)
  {
    $loggedIn = true;
  }
  else
  {
    header("Location: logout.php");
  }
}

if($loggedIn)
{
  $topbar = new TopBar($user->user_id, $user->admin);
}
else
{
  $topbar = new TopBar(-1);
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once("components/head.html");?>

    <title>404 - Page Not Found</title>

    <link href="default.css" rel="stylesheet">
  </head>

  <body>
    <?php echo $topbar;?>
    <div class="main container">
      <h1 class>404 - Page Not Found!</h1>
    </div>
    <!-- Core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="libs/jquery-1.11.0.js"></script>
    <script src="libs/bootstrap-3.1.1-dist//js/bootstrap.min.js"></script>
    <script src="libs/jquery.validate.min.js"></script>
    <script src="js/validate-init.js"></script>
    <script src="js/module-validate.js"></script>
    <script src="js/create-module.js"></script>
  </body>
</html>