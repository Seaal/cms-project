<?php

class FileHandler
{
  const UPLOAD_PATH = "/opt/lampp/htdocs/cms-project/uploads/";


  public function __construct()
  {
  }

  public function createModuleFolders($moduleID)
  {
    mkdir("uploads/".$moduleID, 0777, true);
    mkdir(self::UPLOAD_PATH.$moduleID."/images", 0777, true);
    mkdir(self::UPLOAD_PATH.$moduleID."/files", 0777, true);
  }

  private function remove_recursive($path)
  {
    if (is_dir($path) === true)
    {
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($files as $file)
        {
            $this->remove_recursive(realpath($path) . '/' . $file);
        }

        return rmdir($path);
    }
    else if (is_file($path) === true)
    {
        return unlink($path);
    }

    return false;
  }

  public function deleteModuleFolders($moduleID)
  {
    $this->remove_recursive(self::UPLOAD_PATH.$moduleID);
  }

  private function copy_recursive($source, $dest)
  {
    $dir = opendir($source);

    if(!is_dir($dest))
    {
      mkdir($dest);
    }

    while(false !== ($file = readdir($dir)))
    {
      if($file != "." && $file != "..")
      {
        if(is_dir($source . "/" . $file))
        {
          $this->copy_recursive($source . "/" . $file, $dest . "/" . $file);
        }
        else
        {
          copy($source . "/" . $file, $dest . "/" . $file);
        }
      }
    }

  }

  public function copyModuleFolders($oldModuleID, $newModuleID)
  {
    $this->copy_recursive(self::UPLOAD_PATH.$oldModuleID, self::UPLOAD_PATH.$newModuleID);
  }
}

?>