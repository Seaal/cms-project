<?php

require_once("PageDAO.php");
require_once("Page.php");

class SideBar
{
  //The currently selected module
  private $moduleID;

  //The currently selected page
  private $selectedPageRef;

  //Whether the user is logged in or not
  private $loggedIn;

  private $moduleCode;

  private $year;

  public function __construct($moduleID, $selectedPageRef, $loggedIn, $moduleCode, $year)
  {
    $this->moduleID = $moduleID;
    $this->selectedPageRef = $selectedPageRef;
    $this->loggedIn = $loggedIn;
    $this->moduleCode = $moduleCode;
    $this->year = $year;
  }

  public static function getPageLinkHTML($ref, $title, $active, $buttons, $id, $hidden)
  {
    $HTML = "<li class=\"sidebar-link";

    $HTML .= $hidden ? " side-bar-link-hidden" : "";

    if($active)
    {
      $HTML .= " active";
      $ref = "#";
    }

    $HTML.="\"";

    $HTML.= $id == "" ? "" : " data-page-id=\"".$id."\"";

    $HTML .= "><a href=\"".$ref."\">".htmlentities($title);

    if($buttons)
    {
      $HTML.= "<button type=\"button\" class=\"btn btn-xs btn-info pull-right movePageDown\"><span class=\"glyphicon glyphicon-chevron-down\"></button>
    <button type=\"button\" class=\"btn btn-xs btn-info pull-right movePageUp\"><span class=\"glyphicon glyphicon-chevron-up\"></button>";
    }

    $HTML.= "</a></li>";

    return $HTML;
  }

  private function getPageNavbarLinks()
  {
    $pdao = new PageDAO();
    $pages = $pdao->getPagesFromModule($this->moduleID, $this->loggedIn);

    $HTML = "";

    $ref = "moduleOverview.php?code=".$this->moduleCode."&year=".$this->year;
    $HTML .= SideBar::getPageLinkHTML($ref,"Overview",$this->selectedPageRef=="", false, "", false);

    foreach($pages as $page)
    {
      if(!($page->page_hidden && !$this->loggedIn))
      {
        $ref = "viewPage.php?code=".$this->moduleCode."&ref=".$page->reference."&year=".$this->year;
        $HTML.= SideBar::getPageLinkHTML($ref, $page->title, $this->selectedPageRef == $page->reference, $this->loggedIn, $page->page_id, $page->page_hidden);
      }
    }

    return $HTML;
  }

  public function getHTML()
  {
    $HTML = "<div class=\"col-sm-3 col-md-2 sidebar\">
             <ul class=\"nav nav-sidebar\" id=\"sidebarLinks\">";

    $HTML .= $this->getPageNavbarLinks();

    if($this->loggedIn)
    {
      $HTML .=  "</ul>
                <ul class=\"nav nav-sidebar\">
                <li>Create New Page</li>
                <form class=\"navbar-form navbar-left\" id=\"createPageForm\">
                <div class=\"form-group\">
                <label for=\"createPageTitle\" class=\"control-label sr-only\">Title</label>
                <input type=\"text\" id=\"createPageTitle\" class=\"form-control\" placeholder=\"Title...\" name=\"title\" autocomplete=\"off\">
                </div>
                <div class=\"form-group\">
                <label for=\"createPageReference\" class=\"control-label sr-only\">Reference</label>
                <input type=\"text\" id=\"createPageReference\" class=\"form-control\" placeholder=\"Reference...\" name=\"ref\" autocomplete=\"off\">
                </div>
                <input type=\"hidden\" name=\"module\" value=\"".(int)$this->moduleID."\">
                <input type=\"hidden\" name=\"code\" value=\"".$this->moduleCode."\">
                <input type=\"hidden\" name=\"year\" value=\"".$this->year."\">
                <input type=\"submit\" style=\"position: absolute; left: -9999px; width: 1px; height: 1px;\">
                </form>";
    }

    $HTML .= "</ul>
              </div>";

    return $HTML;
  }

  public function __toString()
  {
    return $this->getHTML();
  }
}

?>