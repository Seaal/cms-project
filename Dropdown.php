<?php

require_once("DropdownLink.php");
require_once("DropdownSeparator.php");
require_once("DropdownLabel.php");

class Dropdown
{
  //The text that you click on to display the drop down
  private $label;

  //The rows which make up the dropdown
  private $rows;

  //The number of rows in the dropdown
  private $numRows;

  public function __construct($label)
  {
    $this->label = $label;
    $this->rows = array();
    $this->numRows = 0;
  }

  public function setLabel($label)
  {
    $this->label = $label;
  }

  public function getLabel()
  {
    return $this->label;
  }

  public function addLink($text, $url)
  {
    $this->rows[$this->numRows] = new DropdownLink($text, $url);
    $this->numRows++;
  }

  public function addSeparator()
  {
    $this->rows[$this->numRows] = new DropdownSeparator();
    $this->numRows++;
  }

  public function addLabel($label)
  {
    $this->rows[$this->numRows] = new DropdownLabel($label);
    $this->numRows++;
  }

  public function getHTML()
  {
    if($this->numRows == 0)
    {
      return "";
    }
    else
    {
      $HTML = "<li class=\"dropdown\">\n
               <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";

      $HTML.= $this->label;

      $HTML.= "<b class=\"caret\"></b></a>\n
              <ul class=\"dropdown-menu\">\n";

      foreach($this->rows as $row)
      {
        $HTML.= $row->getHTML();
      }

      $HTML.="</ul>\n
              </li>";
    }

    return $HTML;
  }

  public function __toString()
  {
    return $this->getHTML();
  }
}

?>