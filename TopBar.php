<?php

require_once("Dropdown.php");
require_once("ModuleDAO.php");

class TopBar
{

  //Whether the user is logged in or not
  private $userID;

  //Whether the user has admin priveleges or not
  private $admin;

  public function __construct($userID = -1, $admin = false)
  {
    $this->userID = $userID;
    $this->admin = $admin;
  }

  //Returns a Dropdown filled with Modules owned by the logged in user
  private function getMyModules()
  {
    $mdao = new ModuleDAO();

    $currentYear = $mdao->getCurrentYear();

    $codes = $mdao->getCodesByUser($this->userID);

    if(empty($codes))
    {
      return "";
    }
    else
    {
      $label = "My Modules";
      $dd = new Dropdown($label);

      foreach($codes as $code)
      {
        $dd->addLink($code,"moduleOverview.php?code=".$code."&year=".$currentYear);
      }
    }

    return $dd->getHTML();
  }

  private function isLoggedIn()
  {
    return $this->userID > -1;
  }

  private function getYearDropdown($year)
  {
    $mdao = new ModuleDAO();

    $currentYear = $mdao->getCurrentYear();

    $modules = $mdao->getCodesSemestersByYear($year, true);

    if(empty($modules))
    {
      return "";
    }
    else
    {
      $label = "Year ".$year;
      $dd = new Dropdown($label);

      $semesters = $mdao->getSemesters();

      $first = true;

      foreach($semesters as $semester)
      {
          if(!empty($modules[$semester->semester_id]))
          {
            if(!$first)
            {
              $dd->addSeparator();
            }
            else
            {
              $first = false;
            }

            $dd->addLabel($semester->name);

            foreach($modules[$semester->semester_id] as $code)
            {
              $dd->addLink($code,"moduleOverview.php?code=".$code."&year=".$currentYear);
            }
        }
      }

      return $dd->getHTML();
    }
  }

  public function getHTML()
  {
    $HTML = "<div class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
          <div class=\"container-fluid\">
          <div class=\"navbar-header\">
          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
          <span class=\"sr-only\">Toggle navigation</span>
          <span class=\"icon-bar\"></span>
          <span class=\"icon-bar\"></span>
          <span class=\"icon-bar\"></span>
          </button>
          <a class=\"navbar-brand\" href=\"index.php\">Home</a>
          </div>
          <div class=\"navbar-collapse collapse\">
          <ul class=\"nav navbar-nav navbar-left\">";

    $HTML .= $this->getYearDropdown(1);
    $HTML .= $this->getYearDropdown(2);
    $HTML .= $this->getYearDropdown(3);
    $HTML .= $this->getYearDropdown(4);

    $HTML .= "</ul>
              <ul class=\"nav navbar-nav navbar-right\">";

    if($this->isLoggedIn())
    {
      if($this->admin)
      {
        $HTML.= "<li><a href=\"admin.php\">Admin Page</a></li>";
      }

      $HTML .= "<li><a href=\"createModule.php\">Create Module</a></li>";
      
      $HTML .= $this->getMyModules();

      $HTML .= "<li><a href=\"logout.php\">Log Out</a></li>";
    }
    else
    {
      $HTML .= "<li><a href=\"https://localhost/cms-project/login.php\">Log In</a></li>";
    }

    $HTML .= "</ul>
          </div>
          </div>
          </div>";

    return $HTML;
  }

  public function __toString()
  {
    return $this->getHTML();
  }
}
?>