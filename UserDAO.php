<?php

require_once("DAO.php");

class UserDAO extends DAO
{
  public function _construct()
  {
  }

  public function getUserByID($userID)
  {
    $conn = $this->getConnection();

    $query = "SELECT * FROM user WHERE user_id = ?";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $userID);

    //Fetch the data into an object
    $statement->setFetchMode(PDO::FETCH_OBJ);

    $statement->execute();

    return $statement->fetch();
  }

  public function getUserIDByLogin($username, $password)
  {
    $conn = $this->getConnection();

    $query = "SELECT user_id, password FROM user WHERE username = ?";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $username);

    $statement->execute();

    $result = $statement->fetch();

    if($result)
    {
      if($result['password'] == $password)
      {
        return $result['user_id'];
      }
      else
      {
        return "Password was incorrect";
      }
    }
    else
    {
      return "Username was incorrect";
    }
  }

  public function getUsersWhoOwnModule($moduleID)
  {
    $conn = $this->getConnection();

    $query = "SELECT user_id FROM user_module WHERE module_id = ?";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $moduleID);

    $statement->execute();

    return $statement->fetchall();
  }

  private function rolloverModuleOwnership($user, $newModuleID)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "INSERT INTO user_module(module_id, user_id)
              VALUES (?,?)";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $newModuleID);
    $statement->bindParam(2, $user[0]);

    $statement->execute();
  }

  public function deleteOwnershipsFromModule($moduleID)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "DELETE FROM user_module WHERE module_id = ?";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $moduleID);

    $statement->execute();
  }

  public function rollover($oldModuleID, $newModuleID)
  {
    $users = $this->getUsersWhoOwnModule($oldModuleID);

    foreach($users as $user)
    {
      $this->rolloverModuleOwnership($user, $newModuleID);
    }

    $this->deleteOwnershipsFromModule($oldModuleID);
  }
}


?>