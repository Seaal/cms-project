<?php
require_once("UserDAO.php");
require_once("ModuleDAO.php");
require_once("ModuleValidator.php");
require_once("FileHandler.php");

require_once("TopBar.php");

session_start();

if(isset($_SESSION["userID"]))
{
  $udao = new UserDAO();
  $user = $udao->getUserByID($_SESSION["userID"]);
  if(!$user)
  {
    header("Location: logout.php");
  }
}
else
{
  header("Location: index.php");
}

$mdao = new ModuleDAO();

$thisYear = $mdao->getCurrentYear();


$submitted = !empty($_POST);

if($submitted)
{
  $formData = $_POST;
  $formData["hidden"] = 1;
  $formData["year"] = $thisYear;

  $validator = new ModuleValidator();

  $result = $validator->validate($formData);

  if($result->getPassed())
  {
    $name = $_POST["name"];
    $code = $_POST["code"];
    $year = $_POST["schoolYear"];
    $semester = $_POST["semester"];

    $moduleID = $mdao->createModule($name, $code, $year, $semester, $user->user_id, $thisYear);

    if($moduleID)
    {
      $fh = new FileHandler();
      $fh->createModuleFolders($moduleID);
      header("Location: moduleOverview.php?code=".$code."&year=".$thisYear);
    }
  }
}

$topbar = new TopBar($user->user_id, $user->admin);

function getSemesterRadios(&$result, $submitted)
{
  $mdao = new ModuleDAO();
  $semesters = $mdao->getSemesters();

  $HTML = "";

  for($i=0;$i<sizeof($semesters);$i++)
  {
    $id = $semesters[$i]->semester_id;
    $name = $semesters[$i]->name;
    $HTML .= "
    <label class=\"radio-inline";

    $HTML.= $i == sizeof($semesters)-1 ? " last-radio" : "";

    $HTML.= "\">
      <input type=\"radio\" name=\"semester\" id=\"semesterRadio".$i."\" value=\"".$id."\"";

    $HTML.= isset($_POST["semester"]) && $_POST["semester"] == $id ? " checked" : "";
    $HTML.= "> ".$name;
    $HTML.= "</label>";
    $HTML.= $submitted && $result->getResult("semester") !== true && $i == sizeof($semesters)-1 ? "<br><span class=\"help-block\">".$result->getResult("semester")."</span>" : "";
  }

  return $HTML;
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once("components/head.html");?>

    <title>Create Module</title>

    <link href="default.css" rel="stylesheet">
  </head>

  <body>
    <?php echo $topbar;?>
    <div class="main container">
      <h1 class="page-header">Create a Module</h1>

      <form class="form-horizontal" id="createModuleForm" role="form" action="createModule.php" method="post">
        <div class="form-group <?php echo $submitted && $result->getResult("name") !== true ? "has-error" : "" ?>">
          <label for="inputName" class="col-sm-2 control-label">Module Name</label>
          <div class="col-sm-5">
            <input type="text" class="form-control" name="name" id="inputName" placeholder="Name" value="<?php echo isset($_POST["name"]) ? $_POST["name"] : "";?>" required autofocus>
            <?php echo $submitted && $result->getResult("name") !== true ? "<span class=\"help-block\">".$result->getResult("name")."</span>" : "";?>
          </div>
        </div>
        <div class="form-group <?php echo $submitted && $result->getResult("code") !== true ? "has-error" : "" ?>">
          <label for="inputCode" class="col-sm-2 control-label">Module Code</label>
          <div class="col-sm-5">
            <input type="text" class="form-control" name="code" id="inputCode" placeholder="Code" value="<?php echo isset($_POST["code"]) ? $_POST["code"] : "";?>" required>
            <?php echo $submitted && $result->getResult("code") !== true ? "<span class=\"help-block\">".$result->getResult("code")."</span>" : "";?>
            <p class="help-block">For Example: COMP12111</p>
          </div>
        </div>
        <div class="form-group <?php echo $submitted && $result->getResult("schoolYear") !== true ? "has-error" : "" ?>">
          <label for="yearRadio1" class="col-sm-2 control-label">Year</label>
          <div class="col-sm-5">
            <label class="radio-inline">
              <input type="radio" name="schoolYear" id="yearRadio1" value="1" <?php echo isset($_POST["schoolYear"]) && $_POST["schoolYear"] == "1" ? "checked" : "" ?>> 1
            </label>
            <label class="radio-inline">
              <input type="radio" name="schoolYear" id="yearRadio2" value="2" <?php echo isset($_POST["schoolYear"]) && $_POST["schoolYear"] == "2" ? "checked" : "" ?>> 2
            </label>
            <label class="radio-inline">
              <input type="radio" name="schoolYear" id="yearRadio3" value="3" <?php echo isset($_POST["schoolYear"]) && $_POST["schoolYear"] == "3" ? "checked" : "" ?>> 3
            </label>
            <label class="radio-inline last-radio">
              <input type="radio" name="schoolYear" id="yearRadio4" value="4" <?php echo isset($_POST["schoolYear"]) && $_POST["schoolYear"] == "4" ? "checked" : "" ?>> 4
            </label>
            <?php echo $submitted && $result->getResult("schoolYear") !== true ? "<span class=\"help-block\">".$result->getResult("schoolYear")."</span>" : "";?>
          </div>
        </div>
         <div class="form-group <?php echo $submitted && $result->getResult("semester") !== true ? "has-error" : "" ?>">
          <label for="semesterRadio1" class="col-sm-2 control-label">Semester</label>
          <div class="col-sm-5">
            <?php echo getSemesterRadios($result, $submitted);?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-5">
            <button type="submit" class="btn btn-primary">Create Module</button>
          </div>
        </div>
      </form>
    </div>
    <!-- Core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="libs/jquery-1.11.0.js"></script>
    <script src="libs/bootstrap-3.1.1-dist//js/bootstrap.min.js"></script>
    <script src="libs/jquery.validate.min.js"></script>
    <script src="js/validate-init.js"></script>
    <script src="js/module-validate.js"></script>
    <script src="js/create-module.js"></script>
    <script>initModuleValidation("","<?php echo $thisYear;?>");</script>
  </body>
</html>