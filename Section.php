<?php
class Section
{
  public $section_id;
  public $page_id;
  public $hidden;
  public $body;
  public $section_order;

  public function __construct($queryData)
  {
    $this->section_id = $queryData['section_id'];
    $this->page_id = $queryData['page_id'];
    $this->hidden = $queryData['hidden'];
    $this->body = $queryData['body'];
    $this->section_order = $queryData['section_order'];
  }

  public function __toString()
  {
    return isset($this->body) ? $this->body : "";
  }
}
?>