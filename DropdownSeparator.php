<?php

require_once("DropdownRow.php");

class DropdownSeparator extends DropdownRow
{
  public function __construct()
  {
  }

  public function getHTML()
  {
    return "<li class=\"divider\"></li>\n";
  }
}
?>