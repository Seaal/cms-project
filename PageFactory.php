<?php

require_once("Factory.php");
require_once("Page.php");
require_once("PageNotFoundException.php");

require_once("restricted/database-config.php");

class PageFactory implements Factory
{
  public function __construct()
  {
  }

  public function make($queryData)
  {
    if(empty($queryData))
    {
      throw new PageNotFoundException();
    }

    $data = array();

    $data['page_id'] = $queryData[0]['page_id'];
    $data['title'] = $queryData[0]['title'];
    $data['reference'] = $queryData[0]['reference'];
    $data['hidden'] = $queryData[0]['page_hidden'];
    $data['page_order'] = $queryData[0]['page_order'];
    $data['sections'] = array();

    for($i = 0; $i < sizeof($queryData); $i++)
    {
      $data['sections'][$i] = new Section($queryData[$i]);
    }

    return new Page($data);
  }
}
?>