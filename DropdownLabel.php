<?php

require_once("DropdownRow.php");

class DropdownLabel extends DropdownRow
{
  private $label;

  public function __construct($label = "")
  {
    $this->label = $label;
  }

  public function getHTML()
  {
    return "<li>".$this->label."</li>\n";
  }
}

?>