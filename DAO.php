<?php

require_once("restricted/database-config.php");

abstract class DAO
{
  protected function getConnection()
  {
    //Create a new database connection
    $conn = new PDO("mysql:host=".DBConfig::HOST.";dbname=".DBConfig::NAME,
                   DBConfig::USER, DBConfig::PASSWORD);

    //Turn on exceptions
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $conn;
  }
}

?>
