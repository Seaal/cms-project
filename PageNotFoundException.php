<?php
class PageNotFoundException extends RuntimeException
{
  public function __construct()
  {
    parent::__construct();
  }
}
?>