<?php
class ModuleNotFoundException extends RuntimeException
{
  public function __construct()
  {
    parent::__construct();
  }
}
?>