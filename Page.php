<?php

require_once("PageNotFoundException.php");
require_once("Section.php");

class Page
{
  //Data About the page
  public $page_id;
  public $title;
  public $reference;
  public $hidden;
  public $created_by;
  public $created_time;
  public $last_edited_by;
  public $last_edited_time;
  public $sections;
  public $page_order;

  private $loggedIn;

  public function __construct($data)
  {
    $this->page_id = $data['page_id'];
    $this->title = $data['title'];
    $this->reference = $data['reference'];
    $this->hidden = $data['hidden'];
    $this->page_order = $data['page_order'];
    $this->sections = $data['sections'];

    $this->loggedIn = false;
  }

  public function getLoggedIn()
  {
    return $this->loggedIn;
  }

  public function setLoggedIn($loggedIn)
  {
    $this->loggedIn = $loggedIn;
  }

  public function getJavascriptInit($moduleCode, $moduleID, $year)
  {
    return "initPage('".$this->page_id."','".$this->getPreviewButton()."','".$moduleCode."','".$moduleID."','".$year."','".$this->reference."');";
  }

  public static function getShowButton()
  {
    return "<button type=\"button\" class=\"btn btn-sm btn-info visibility-btn showSection\">Show <span class=\"glyphicon glyphicon-plus\"></button>";
  }

  public static function getHideButton()
  {
    return "<button type=\"button\" class=\"btn btn-sm btn-info visibility-btn hideSection\">Hide <span class=\"glyphicon glyphicon-minus\"></button>";
  }

  private function getPreviewButton()
  {
    return "<button type=\"button\" class=\"btn btn-warning header-btn pull-right\" id=\"previewPage\">Preview <span class=\"glyphicon glyphicon-play-circle\"></span></button>";
  }

  public static function getEditingSectionHTML($section)
  {
    $HTML = "
      <div class=\"section\" id=\"".$section->section_id."\">
      <div class=\"editable";

    $HTML .= $section->hidden ? " section-hidden" : "";

    $HTML.= "\" style=\"width:100%\">";

    $HTML .= $section == "" ? "<p>[Empty Section] (Click to Edit)</p>" : $section;
    
    $HTML .= "
        </div>
        <button type=\"button\" class=\"btn btn-sm btn-success saveSection\">Save <span class=\"glyphicon glyphicon-ok\"></span></button>
        <button type=\"button\" class=\"btn btn-sm btn-danger deleteSection\">Delete <span class=\"glyphicon glyphicon-remove\"></button>\n";
        
    if($section->hidden)
    {
      $HTML .= Page::getShowButton();
    }
    else
    {
      $HTML .= Page::getHideButton();
    }
        
    $HTML .= "
      <button type=\"button\" class=\"btn btn-sm btn-primary moveSectionUp\">Move Up <span class=\"glyphicon glyphicon-chevron-up\"></button>
      <button type=\"button\" class=\"btn btn-sm btn-primary moveSectionDown\">Move Down <span class=\"glyphicon glyphicon-chevron-down\"></button>
      </div>";

    return $HTML;
  }

  public static function getTinyMCEInitJavascript()
  {
    return "<script>initTinyMce();</script>";
  }

  private function getEditPageForm()
  {
    $HTML= "<form class=\"form-horizontal\" id=\"editPageForm\" role=\"form\">
            <div class=\"form-group\">
              <label for=\"inputTitle\" class=\"col-sm-2 control-label\">Page Title</label>
              <div class=\"col-sm-5\">
                <input type=\"text\" class=\"form-control\" name=\"title\" id=\"inputTitle\" placeholder=\"Title\" value=\"".$this->title."\" required>
              </div>
            </div>
            <div class=\"form-group\">
              <label for=\"inputRef\" class=\"col-sm-2 control-label\">Page Reference</label>
              <div class=\"col-sm-5\">
                <input type=\"text\" class=\"form-control\" name=\"ref\" id=\"inputRef\" placeholder=\"Reference\" value=\"".$this->reference."\" required>
              </div>
            </div>
            <div class=\"form-group\">
              <label for=\"visibilityRadio1\" class=\"col-sm-2 control-label\">Visibility</label>
              <div class=\"col-sm-5\">
                <label class=\"radio-inline\">
                  <input type=\"radio\" name=\"hidden\" id=\"visibilityRadio1\" ";
                  $HTML .= !$this->hidden ? " checked " : "";
                  $HTML .= "value=\"0\"> Visible
                </label>
                <label class=\"radio-inline\">
                  <input type=\"radio\" name=\"hidden\" id=\"visibilityRadio2\" ";
                  $HTML .= $this->hidden ? " checked " : "";
                  $HTML .= "value=\"1\"> Hidden
                </label>
              </div>
            </div>
            <div class=\"form-group\">
              <div class=\"col-sm-offset-2 col-sm-5\">
                <button type=\"submit\" id=\"submitEditPage\"class=\"btn btn-primary\">Edit Page <span class=\"glyphicon glyphicon-pencil\"></span></button>
              </div>
            </div>
            <hr>
          </form>";

    return $HTML;
  }

  public function getHTML($moduleName)
  {
    $HTML = "";

    if($this->loggedIn)
    {
      $HTML.= "
        <h1 class=\"page-header\">".htmlentities($moduleName)."<br><small>".htmlentities($this->title).$this->getPreviewButton()."</small>
        <button type=\"button\" class=\"btn btn-danger header-btn pull-right\" id=\"deletePage\">Delete Page <span class=\"glyphicon glyphicon-remove\"></span></button>
        <button type=\"button\" class=\"btn btn-primary header-btn pull-right\" id=\"editPage\">Edit Page <span class=\"glyphicon glyphicon-pencil\"></span></button></h1>
        <div id=\"errorMessage\"></div>";

      $HTML.= $this->getEditPageForm();
        
      $HTML.= "<script src=\"libs/tinymce/tinymce.min.js\"></script>
        <script src=\"js/tinymce-init.js\"></script>
        <div id=\"content\">";

      $HTML.= Page::getTinyMCEInitJavascript();

      $HTML.= "<div id=\"sections\">";

      foreach($this->sections as $section)
      {
        $HTML .= Page::getEditingSectionHTML($section);
      }

      $HTML.= "
        </div>
        <button type=\"button\" id=\"createNewSection\" class=\"btn btn-warning\">Create Section <span class=\"glyphicon glyphicon-pencil\"></button>
        <button type=\"button\" id=\"saveAllSections\"class=\"btn btn-success\">Save All <span class=\"glyphicon glyphicon-ok\"></span></button>
        </div>";
    }
    else
    { 
      $HTML.= "<h1 class=\"page-header\">".htmlentities($moduleName)."<br><small>".htmlentities($this->title)."</small></h1>";

      foreach($this->sections as $section)
      {
        $HTML .= $section;
      }
    }
    
    return $HTML;
  }
}
?>