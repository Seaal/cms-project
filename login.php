<?php
require_once("TopBar.php");
require_once("UserDAO.php");

session_start();

if(isset($_SESSION["userID"]))
{
  header("Location: index.php");
}

$topbar = new TopBar(-1);

$errorMessage = "";

if(isset($_POST["username"]) && isset($_POST["password"]))
{
  $udao = new UserDAO();

  $userID = $udao->getUserIDByLogin($_POST["username"], $_POST["password"]);

  if(is_numeric($userID))
  {
    $_SESSION["userID"] = $userID;

    header("Location: index.php");
  }
  else
  {
    $errorMessage = $userID;
  }
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once("components/head.html");?>

    <title>Log in</title>

    <link href="default.css" rel="stylesheet">
    <link href="login.css" rel="stylesheet">
  </head>

  <body>
    <?php echo $topbar;?>

    <div class="container-fluid">
      <form class="form-signin" role="form" method="post" action="login.php">
        <h2 class="form-signin-heading">Log in (Staff Only)</h2>
        <input type="username" name="username" class="form-control" placeholder="Username" required autofocus>
        <input type="password" name="password" class="form-control" placeholder="Password" required autocomplete="off">
        <?php
        if($errorMessage!="")
        {
          echo "<div class=\"alert alert-danger\" align=\"center\">".$errorMessage." <span class=\"glyphicon glyphicon-warning-sign\"></span></div>";
        }
        ?>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
        
      </form>
    </div>

    <!-- Core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="libs/jquery-1.11.0.js"></script>
    <script src="libs/bootstrap-3.1.1-dist//js/bootstrap.min.js"></script>
  </body>
</html>