<?php

require_once("DAO.php");
require_once("SectionDAO.php");

require_once("Page.php");
require_once("PageFactory.php");
require_once("PageNotFoundException.php");

class PageDAO extends DAO
{
  public function __construct()
  {
  }

  public function getPageByRef($reference, $showHidden, $moduleID)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();

      $query = "SELECT * FROM page LEFT JOIN section
                ON page.page_id = section.page_id
                WHERE reference = ? AND module_id = ?";

      if(!$showHidden)
      {
        $query .= " AND page_hidden = 0 AND hidden = 0";
      }

      $query .= " ORDER BY section_order";

      $statement = $conn->prepare($query);

      //Fetch the data into an array
      $statement->setFetchMode(PDO::FETCH_ASSOC);

      //Bind the reference of the page to avoid injection
      $statement->bindParam(1, $reference);
      $statement->bindParam(2, $moduleID);

      $statement->execute();

      $result = $statement->fetchall();

      $pfact = new PageFactory();

      return $pfact->make($result);
    }
    catch(PDOException $e)
    {  
      echo $e->getMessage();
    }
    catch(PageNotFoundException $e)
    {
      throw new PageNotFoundException();
    }
  }

  //Show/Hide page from public view
  public function setPageVisibility($page, $hidden)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();

      $query = "UPDATE page SET page_hidden = ? WHERE page_id = ?";

      $statement = $conn->prepare($query);

      //Bind the reference of the page to avoid injection
      $statement->bindParam(1, $hidden);
      $statement->bindParam(2, $page->page_id);

      $statement->execute();

      $page->hidden = $hidden;
    }
    catch(PDOException $e)
    {  
      echo $e->getMessage();
    }
  }

  public function getPagesFromModule($module_id, $showHidden)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();

      $query = "SELECT * FROM page WHERE module_id = ?";

      if(!$showHidden)
      {
        $query .= " AND page_hidden = 0";
      }

      $query .= " ORDER BY page_order";

      $statement = $conn->prepare($query);

      //Fetch the data into an array
      $statement->setFetchMode(PDO::FETCH_OBJ);

      //Bind the reference of the page to avoid injection
      $statement->bindParam(1, $module_id);

      $statement->execute();

      $pages = array();

      while($page = $statement->fetch())
      {
        $pages[sizeof($pages)] = $page;
      }

      return $pages;
    }
    catch(PDOException $e)
    {  
      echo $e->getMessage();
    }
    catch(PageNotFoundException $e)
    {
      echo "Page Not Found!";
    }
  }

  public function createPage($title, $reference, $moduleID)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();


      //**CREATE THE PAGE FIRST**
      
      $statement = $conn->prepare("INSERT INTO page (module_id, title,
                                   reference)
                                   VALUES (?, ?, ?)");

      $statement->bindParam(1, $moduleID);
      $statement->bindParam(2, $title);
      $statement->bindParam(3, $reference);

      $success = $statement->execute();

      $pageID = $conn->lastInsertID();
      
      //Set the order of the page, appears at the bottom

      $statement = $conn->prepare("UPDATE page SET page_order = ?
                                   WHERE page_id = ?");

      $statement->bindParam(1, $pageID);
      $statement->bindParam(2, $pageID);

      $statement->execute();

      //**NOW CREATE A SECTION FOR THE PAGE**
      
      $sdao = new SectionDAO();

      $sdao->createSection($pageID);

      return $pageID;
    }
    catch(PDOException $e)
    {  
      echo $e->getMessage();
    }
  }

  public function editPage($pageID, $title, $newRef, $hidden)
  {
    //Create a new database connection
    $conn = $this->getConnection();
    
    $statement = $conn->prepare("UPDATE page SET
                                 title = ?,
                                 reference = ?,
                                 page_hidden = ?
                                 WHERE page_id = ?");

    $statement->bindParam(1, $title);
    $statement->bindParam(2, $newRef);
    $statement->bindParam(3, $hidden);
    $statement->bindParam(4, $pageID);

    return $statement->execute();
  }

  public function deletePage($pageID)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $statement = $conn->prepare("DELETE FROM page WHERE page_id = ?");

    $statement->bindParam(1, $pageID);

    return $statement->execute();
  }

  public function swapPageOrder($page_id1, $page_id2)
  {
    $conn = $this->getConnection();

    $statement = $conn->prepare("SELECT page_order FROM page WHERE page_id = ? OR page_id = ? ORDER BY page_order");

    $statement->bindParam(1, $page_id1);
    $statement->bindParam(2, $page_id2);

    $statement->execute();

    $order1 = $statement->fetch()['page_order'];
    $order2 = $statement->fetch()['page_order'];

    $statement = $conn->prepare("UPDATE page SET page_order =
                                CASE
                                  WHEN page_id = ? then ?
                                  WHEN page_id = ? then ?
                                END
                                WHERE page_id in (?, ?)");

    $statement->bindParam(1, $page_id1);
    $statement->bindParam(2, $order2);
    $statement->bindParam(3, $page_id2);
    $statement->bindParam(4, $order1);
    $statement->bindParam(5, $page_id1);
    $statement->bindParam(6, $page_id2);

    return $statement->execute();
  }

  private function rolloverPage($page, $newModuleID)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "INSERT INTO page(module_id, title, reference, page_hidden, page_order)
                VALUES (?,?,?,?,?)";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $newModuleID);
    $statement->bindParam(2, $page->title);
    $statement->bindParam(3, $page->reference);
    $statement->bindParam(4, $page->page_hidden);
    $statement->bindParam(5, $page->page_order);

    $success = $statement->execute();

    $newPageID = $conn->lastInsertID();

    if($success)
    {
      $sdao = new SectionDAO();

      $sdao->rollover($page->page_id, $newPageID);
    }
  }

  public function rollover($oldModuleID, $newModuleID)
  {
    $pages = $this->getPagesFromModule($oldModuleID, true);

    foreach($pages as $page)
    {
      $this->rolloverPage($page, $newModuleID);
    }
  }
}

?>