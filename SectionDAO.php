<?php

require_once("DAO.php");

class SectionDAO extends DAO
{
  public function __construct()
  {
  }

  //Creates a new section for the given page ID. Returns the ID of the section
  public function createSection($page_id)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();

      $statement = $conn->prepare("INSERT INTO section(page_id, hidden)
                                   VALUES (?, 0)");

      $statement->bindParam(1, $page_id);

      $success = $statement->execute();

      if($success)
      {
        $statement = $conn->prepare("UPDATE section SET section_order = section_id WHERE section_id = LAST_INSERT_ID()");
        $statement->execute();

        $statement = $conn->prepare("SELECT * FROM section WHERE section_id = LAST_INSERT_ID()");
        $statement->execute();

        return new Section($statement->fetch());
      }
      else
      {
        return false; 
      }
    }
    catch(PDOException $e)
    {  
      throw $e;
    }
  }

  //Deletes a section with the given section ID. Returns true if successful,
  //false otherwise
  public function deleteSection($section_id)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();

      $statement = $conn->prepare("DELETE FROM section WHERE section_id = ?");

      $statement->bindParam(1, $section_id);

      return $statement->execute();
    }
    catch(PDOException $e)
    {  
      throw $e;
    }
  }

  public function editSection($section_id, $data)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();

      $statement = $conn->prepare("UPDATE section SET body = ? WHERE section_id = ?");

      $statement->bindParam(1, $data);
      $statement->bindParam(2, $section_id);

      return $statement->execute();
    }
    catch(PDOException $e)
    {  
      throw $e;
    }
  }

  public function setSectionVisibility($section_id, $hidden)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();

      $statement = $conn->prepare("UPDATE section SET hidden = ? WHERE section_id = ?");

      $statement->bindParam(1, $hidden);
      $statement->bindParam(2, $section_id);

      return $statement->execute();
    }
    catch(PDOException $e)
    {  
      throw $e;
    }
  }

  public function swapSectionOrder($section_id1, $section_id2)
  {
    try
    {
      //Create a new database connection
      $conn = $this->getConnection();

      $statement = $conn->prepare("SELECT section_order FROM section WHERE section_id = ? OR section_id = ? ORDER BY section_order");

      $statement->bindParam(1, $section_id1);
      $statement->bindParam(2, $section_id2);

      $statement->execute();

      $order1 = $statement->fetch()['section_order'];
      $order2 = $statement->fetch()['section_order'];

      $statement = $conn->prepare("UPDATE section SET section_order =
                                  CASE
                                    WHEN section_id = ? then ?
                                    WHEN section_id = ? then ?
                                  END
                                  WHERE section_id in (?, ?)");

      $statement->bindParam(1, $section_id1);
      $statement->bindParam(2, $order2);
      $statement->bindParam(3, $section_id2);
      $statement->bindParam(4, $order1);
      $statement->bindParam(5, $section_id1);
      $statement->bindParam(6, $section_id2);

      return $statement->execute();
    }
    catch(PDOException $e)
    {  
      throw $e;
    }
  }

  public function getSectionsByID($pageID, $hidden)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "SELECT * FROM section WHERE page_id = ?";

    if(!$hidden)
    {
      $query.= " AND hidden = 0";
    }

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $pageID);

    $statement->setFetchMode(PDO::FETCH_OBJ);

    $statement->execute();

    return $statement->fetchall();
  }

  private function rolloverSection($section, $newPageID)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "INSERT INTO section(page_id, hidden, body, section_order)
              VALUES (?,?,?,?)";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $newPageID);
    $statement->bindParam(2, $section->hidden);
    $statement->bindParam(3, $section->body);
    $statement->bindParam(4, $section->section_order);

    $statement->execute();
  }

  public function rollover($oldPageID, $newPageID)
  {
    $sections = $this->getSectionsByID($oldPageID,true);

    foreach($sections as $section)
    {
      $this->rolloverSection($section, $newPageID);
    }
  }
}

?>