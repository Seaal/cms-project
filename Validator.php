<?php

abstract class Validator
{
  public abstract function validate($formData);

  protected function required(&$data)
  {
    if(isset($data) && $data != "")
    {
      return true;
    }
    else
    {
      return "This field is required.";
    }
  }

  protected function maxLength($data, $length)
  {
    if(strlen($data) <= $length)
    {
      return true;
    }
    else
    {
      return "Please enter no more than ".$length." characters.";
    }
  }
}

?>