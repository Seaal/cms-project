Third Year Project - A Content Management System for Module Webpages

This project aims to create an easy to use Content Management System as a way to give information to students about their modules.

Information:

The project uses a MySQL database, using PDO as a database driver.

The database creation script can be found in: test/database-test.php

For the file manager to work, you need to create an 'uploads' directory within the root, and within that a 'thumbs' directory. Both need to be world accessible, i.e. chmod 777 uploads chmod 777 uploads/thumbs

Some of the directories in the File Manager, FileHandler and TopBar (and maybe elsewhere) are hard coded. The project also sat in a /cms-project/ folder.

Libraries used:

Bootstrap
jQuery
jQuery validate
TinyMCE
Responsive File Manager