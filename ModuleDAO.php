<?php

require_once("DAO.php");
require_once("PageDAO.php");
require_once("UserDAO.php");
require_once("ModuleNotFoundException.php");
require_once("FileHandler.php");

class ModuleDAO extends DAO
{
  public function __construct()
  {
  }

  public function getCurrentYear()
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "SELECT current_year FROM settings";

    $statement = $conn->prepare($query);

    $statement->execute();

    return $statement->fetch()[0];
  }

  public function setCurrentYear($year)
  {
    $conn = $this->getConnection();

    $query = "UPDATE settings SET current_year = ?";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $year);

    $statement->execute();
  }

  public function getModuleByCode($code, $showHidden, $year)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "SELECT * FROM module WHERE code = ? AND year = ?";

    if(!$showHidden)
    {
      $query .= " AND hidden = 0";
    }

    $statement = $conn->prepare($query);

    //Fetch the data into an object
    $statement->setFetchMode(PDO::FETCH_OBJ);

    //Bind the reference of the page to avoid injection
    $statement->bindParam(1, $code);
    $statement->bindParam(2, $year);

    $statement->execute();

    $result = $statement->fetch();

    if(!$result)
    {
      throw new ModuleNotFoundException();
    }
    else
    {
      return $result;
    }
  }

  public function getUserOwnsModule($userID, $moduleID)
  {
    $conn = $this->getConnection();

    $query = "SELECT COUNT(*) FROM user_module
              WHERE user_id = ? AND module_id = ?";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $userID);
    $statement->bindParam(2, $moduleID);

    $statement->execute();

    return $statement->fetch()[0];
  }

  public function getCodesByUser($userID)
  {
    $conn = $this->getConnection();

    $query = "SELECT code FROM user_module INNER JOIN module
              ON module.module_id = user_module.module_id
              WHERE user_id = ?";

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $userID);

    $statement->execute();

    $result = $statement->fetchall();

    if(empty($result))
    {
      return $result;
    }

    $codes = array();

    foreach($result as $code)
    {
      $codes[] = $code['code'];
    }

    return $codes;
  }

  //Gets each modules code and semester for navigation
  public function getCodesSemestersByYear($year, $hidden)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "SELECT code, semester FROM module WHERE school_year = ? 
              AND year = ?";

    $query .= $hidden ? "AND hidden = 0 " : "";

    $query .="ORDER BY code";

    $statement = $conn->prepare($query);

    //Fetch the data into an object
    $statement->setFetchMode(PDO::FETCH_OBJ);

    //Bind the reference of the page to avoid injection
    $statement->bindParam(1, $year);
    $statement->bindParam(2, $this->getCurrentYear());


    $statement->execute();

    $result = $statement->fetchall();

    if(empty($result))
    {
      return $result;
    }

    $modules = array();

    $semesters = $this->getSemesters();

    foreach($semesters as $semester)
    {
      $modules[$semester->semester_id] = array();
    }

    $modules[1] = array();
    $modules[2] = array();
    $mdoules[3] = array();

    foreach($result as $module)
    {
      $modules[$module->semester][] = $module->code;
    }

    return $modules;
  }

  //Returns all the semesters
  public function getSemesters()
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "SELECT * FROM semester ORDER BY semester_id";

    $statement = $conn->prepare($query);

    //Fetch the data into an array
    $statement->setFetchMode(PDO::FETCH_OBJ);

    $statement->execute();

    $result = $statement->fetchall();

    return $result;
  }

  public function createModule($name, $code, $schoolYear, $semester, $userID, $year)
  {
    //Create a new database connection
    $conn = $this->getConnection();
    
    $statement = $conn->prepare("INSERT INTO module (name, code,
                                 school_year, semester, year)
                                 VALUES (?, ?, ?, ?, ?)");

    $statement->bindParam(1, $name);
    $statement->bindParam(2, $code);
    $statement->bindParam(3, $schoolYear);
    $statement->bindParam(4, $semester);
    $statement->bindParam(5, $year);

    $success = $statement->execute();

    if($success)
    {

      $moduleID = $conn->lastInsertID();

      $statement = $conn->prepare("INSERT INTO user_module (module_id, user_id)
                                   VALUES (?, ?)");

      $statement->bindParam(1, $moduleID);
      $statement->bindParam(2, $userID);

      $statement->execute();

      return $moduleID;
    }

    return $success;
  }

  public function editModule($oldCode, $name, $newCode, $schoolYear, $semester, $hidden, $year)
  {
    //Create a new database connection
    $conn = $this->getConnection();
    
    $statement = $conn->prepare("UPDATE module SET
                                 name = ?,
                                 code = ?,
                                 school_year = ?,
                                 semester = ?,
                                 hidden = ?
                                 WHERE code = ? AND year = ?");

    $statement->bindParam(1, $name);
    $statement->bindParam(2, $newCode);
    $statement->bindParam(3, $schoolYear);
    $statement->bindParam(4, $semester);
    $statement->bindParam(5, $hidden);
    $statement->bindParam(6, $oldCode);
    $statement->bindParam(7, $year);

    return $statement->execute();
  }

  public function deleteModule($moduleID)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $statement = $conn->prepare("DELETE FROM module WHERE module_id = ?");

    $statement->bindParam(1, $moduleID);

    return $statement->execute();
  }

  public function getModulesByYear($year, $hidden)
  {
    //Create a new database connection
    $conn = $this->getConnection();

    $query = "SELECT * FROM module WHERE year = ?";

    if(!$hidden)
    {
      $query.= " AND hidden = 0";
    }

    $statement = $conn->prepare($query);

    $statement->bindParam(1, $year);

    $statement->setFetchMode(PDO::FETCH_OBJ);

    $statement->execute();

    return $statement->fetchall();
  }

  private function rolloverModule($module)
  {
    $conn = $this->getConnection();

    $query = "INSERT INTO module(name, code, school_year, semester, year, hidden)
                VALUES (?,?,?,?,?,?)";

    $statement = $conn->prepare($query);

    $newYear = $module->year+1;

    $statement->bindParam(1, $module->name);
    $statement->bindParam(2, $module->code);
    $statement->bindParam(3, $module->school_year);
    $statement->bindParam(4, $module->semester);
    $statement->bindParam(5, $newYear);
    $statement->bindParam(6, $module->hidden);

    $success = $statement->execute();

    $newModuleID = $conn->lastInsertID();

    if($success)
    {
      $pdao = new PageDAO();

      $pdao->rollover($module->module_id, $newModuleID);

      $udao = new UserDAO();

      $udao->rollover($module->module_id, $newModuleID);

      $fh = new FileHandler();

      $fh->copyModuleFolders($module->module_id, $newModuleID);
    }
  }

  public function rollover()
  {
    $conn = $this->getConnection();

    try
    {
      $conn->beginTransaction();

      $currentYear = $this->getCurrentYear();

      $modules = $this->getModulesByYear($currentYear, true);

      foreach($modules as $module)
      {
        $this->rolloverModule($module);
      }
      
      $this->setCurrentYear($currentYear+1);

      $conn->commit();
    }
    catch(PDOException $e)
    {
      $conn->rollback();
    }
  }
}

?>