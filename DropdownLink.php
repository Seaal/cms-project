<?php

require_once("DropdownRow.php");

class DropdownLink extends DropdownRow
{
  private $text;
  private $url;

  public function __construct($text = "", $url = "#")
  {
    $this->text = $text;
    $this->url = $url;
  }

  public function getHTML()
  {
    return "<li><a href=\"".$this->url."\">".$this->text."</a></li>\n";
  }
}

?>