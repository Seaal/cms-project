<?php

require_once("../ModuleDAO.php");
require_once("../ModuleNotFoundException.php");

if(isset($_GET["code"]) && isset($_GET["year"]))
{

  $code = $_GET["code"];
  $year = $_GET["year"];
  $oldCode = &$_GET["oldCode"];

  if(isset($oldCode) && $oldCode == $code)
  {
    echo 'true';
  }
  else
  {
    try
    {
      $mdao = new ModuleDAO();
      $mdao->getModuleByCode($code, true, $year);
      echo 'false';
    }
    catch(ModuleNotFoundException $e)
    {
      echo 'true';
    }
  }  
}
else
{
  echo 'false';
}
?>