<?php

require_once("../PageDAO.php");
require_once("../PageNotFoundException.php");

if(isset($_GET["ref"]) && isset($_GET["module"]))
{

  $ref = $_GET["ref"];
  $moduleID = $_GET["module"];
  $oldRef = &$_GET["oldRef"];

  if(isset($oldRef) && $oldRef == $ref)
  {
    echo 'true';
  }
  else
  {
    try
    {
      $pdao = new PageDAO();
      $pdao->getPageByRef($ref, true, $moduleID);
      echo 'false';
    }
    catch(PageNotFoundException $e)
    {
      echo 'true';
    }
  } 
}
else
{
  echo 'false';
}
?>