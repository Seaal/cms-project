<?php

require_once("../PageDAO.php");
require_once("../SideBar.php");
require_once("../Alert.php");
require_once("../PageValidator.php");

if(!empty($_POST))
{
  $formData = $_POST;
  $formData["hidden"] = 1;

  $validator = new PageValidator();

  $result = $validator->validate($formData);

  if($result->getPassed())
  {
    $pdao = new PageDAO();

    try
    {
      $moduleID = $_POST['module'];
      $title = $_POST['title'];
      $ref = $_POST['ref'];
      $code = $_POST['code'];
      $year = $_POST['year'];

      $pageID = $pdao->createPage($title, $ref, $moduleID);

      $ref = "viewPage.php?code=".$code."&ref=".$ref."&year=".$year;
      echo SideBar::getPageLinkHTML($ref, $title, false, true, $pageID, true);
    }
    catch(Exception $e)
    {
      echo new Alert("An error has occured.","danger");
    }
  }
  else
  {
    foreach($result->getResults() as $key => $message)
    {
      if($message !== true)
      {
        echo new Alert($message,"danger");
      }
    }
  }
}
?>