<?php

require_once("../SectionDAO.php");
require_once("../Page.php");
require_once("../Alert.php");

if(isset($_POST['section']) && isset($_POST['hidden']))
{
  $section = $_POST['section'];
  $hidden = $_POST['hidden'];

  $sdao = new SectionDAO();

  try
  {
    $sdao->setSectionVisibility($section, $hidden);

    if($hidden)
    {
      echo Page::getShowButton();
    }
    else
    {
      echo Page::getHideButton();
    }
  }
  catch(Exception $e)
  {
    echo new Alert("An error has occured.","danger");
  }
}
else
{
  echo new Alert("Error: Bad request.","danger");
}
?>