<?php

require_once("../SectionDAO.php");
require_once("../Alert.php");

if(isset($_POST['section']) && isset($_POST['data']))
{
  $section = $_POST['section'];
  $data = $_POST['data'];

  $sdao = new SectionDAO();

  try
  {
    $sdao->editSection($section, $data);
    echo new Alert("Successfully Saved.","success");
  }
  catch(Exception $e)
  {
    echo new Alert("An error has occured.","danger");
  }
}
else
{
  echo new Alert("Error: Bad request.","danger");
}
?>