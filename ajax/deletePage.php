<?php

require_once("../PageDAO.php");
require_once("../Alert.php");

if(isset($_POST['page']))
{
  $page = $_POST['page'];

  $pdao = new PageDAO();

  try
  {
    echo $pdao->deletePage($page);
  }
  catch(Exception $e)
  {
    echo new Alert("An error has occured.","danger");
  }
}
else
{
  echo new Alert("Error: Bad request.","danger");
}
?>