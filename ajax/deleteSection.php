<?php

require_once("../SectionDAO.php");
require_once("../Alert.php");

if(isset($_POST['section']))
{
  $section = $_POST['section'];

  $sdao = new SectionDAO();

  try
  {
    $sdao->deleteSection($section);
    echo 'true';
  }
  catch(Exception $e)
  {
    echo new Alert("An error has occured.","danger");
  }
}
else
{
  echo new Alert("Error: Bad request.","danger");
}
?>