<?php

require_once("../ModuleDAO.php");
require_once("../Alert.php");
require_once("../FileHandler.php");

if(isset($_POST['module']))
{
  $module = $_POST['module'];

  $mdao = new ModuleDAO();

  try
  {
    $mdao->deleteModule($module);
    $fh = new FileHandler();
    $fh->deleteModuleFolders($module);

    echo 'true';
  }
  catch(Exception $e)
  {
    echo new Alert("An error has occured.","danger");
  }
}
else
{
  echo new Alert("Error: Bad request.","danger");
}
?>