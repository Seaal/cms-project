<?php

require_once("../SectionDAO.php");
require_once("../Alert.php");

if(isset($_POST['firstSection']) && isset($_POST['secondSection']))
{
  $section1 = $_POST['firstSection'];
  $section2 = $_POST['secondSection'];

  $sdao = new SectionDAO();

  try
  {
    $sdao->swapSectionOrder($section1, $section2);
    echo 'true';
  }
  catch(Exception $e)
  {
    echo new Alert("An error has occured.","danger");
  }
}
else
{
  echo new Alert("Error: Bad request.","danger");
}
?>