<?php

require_once("../PageDAO.php");
require_once("../Alert.php");
require_once("../PageValidator.php");

if(!empty($_POST))
{
  $formData = $_POST;

  $validator = new PageValidator();

  $result = $validator->validate($formData);

  if($result->getPassed())
  {
    try
    {
      $title = $_POST["title"];
      $ref = $_POST["ref"];
      $hidden = $_POST["hidden"];
      $pageID = $_POST["page"];

      $pdao = new PageDAO();
      $success = $pdao->editPage($pageID, $title, $ref, $hidden);
      echo $ref;
    }
    catch(Exception $e)
    {
      echo new Alert("An error has occured.","danger");
    }
  }
  else
  {
    foreach($result->getResults() as $key => $message)
    {
      if($message !== true)
      {
        echo new Alert($message,"danger");
      }
    }
  }
}
?>