<?php

require_once("../PageDAO.php");
require_once("../Alert.php");

if(isset($_POST['firstPage']) && isset($_POST['secondPage']))
{
  $page1 = $_POST['firstPage'];
  $page2 = $_POST['secondPage'];

  $pdao = new PageDAO();

  try
  {
    echo $pdao->swapPageOrder($page1, $page2);
  }
  catch(Exception $e)
  {
    echo new Alert("An error has occured.","danger");
  }
}
else
{
  echo new Alert("Error: Bad request.","danger");
}
?>