<?php

require_once("../SectionDAO.php");
require_once("../Page.php");
require_once("../Alert.php");

if(isset($_POST['page']))
{
  $page = $_POST['page'];

  $sdao = new SectionDAO();

  try
  {
    $newSection = $sdao->createSection($page);

    echo Page::getEditingSectionHTML($newSection);
    echo Page::getTinyMCEInitJavascript();
  }
  catch(Exception $e)
  {
    echo new Alert("An error has occured.","danger");
  }
}
else
{
  echo new Alert("Error: Bad request.","danger");
}

?>