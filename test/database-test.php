<?php

require_once("../restricted/database-config.php");

function performQuery()
{
  if(isset($_POST['action']))
  {

    $action = $_POST['action'];

    $message = '';

    if($action == 'create')
    {

      //Connect to MySQL
      $link = mysql_connect(DBConfig::HOST,DBConfig::USER,DBConfig::PASSWORD);
      $message .= 'Connecting to MySQL: ';

      if(mysql_error() == '')
      {
        $message .= 'Success<br>';
      }
      else
      {
        $message .= mysql_error();
        return $message;
      }

      //Create a new database under the same name
      mysql_query("CREATE DATABASE ".DBConfig::NAME,$link);
      $message .= 'Creating Database: ';
      
      if(mysql_error() == '')
      {
        $message .= 'Success<br>';
      }
      else
      {
        $message .= mysql_error();
        return $message;
      }

      //Select the new database
      mysql_select_db(DBConfig::NAME,$link);
      $message .= 'Selecting Database: ';

      if(mysql_error() == '')
      {
        $message .= 'Success<br>';
      }
      else
      {
        $message .= mysql_error();
        return $message;
      }

      //***PREPARE QUERIES TO CREATE DATABASE TABLES

      $query = array();

      $query[0] = "CREATE TABLE page
      (
        page_id mediumint UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
        module_id mediumint UNSIGNED NOT NULL,
        title varchar(40) NOT NULL,
        reference varchar(40) NOT NULL,
        page_hidden boolean NOT NULL DEFAULT 1,
        page_order mediumint UNSIGNED NOT NULL,
        PRIMARY KEY(page_id)
      )ENGINE=INNODB";

      $query[1] = "CREATE TABLE section
      (
        section_id mediumint UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
        page_id mediumint UNSIGNED NOT NULL,
        hidden boolean NOT NULL,
        body text,
        section_order mediumint UNSIGNED NOT NULL,
        PRIMARY KEY(section_id)
      )ENGINE=INNODB";

      $query[2] = "CREATE TABLE module
      (
        module_id mediumint UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
        name varchar(100) NOT NULL,
        code varchar(15) NOT NULL,
        school_year smallint NOT NULL,
        semester smallint UNSIGNED NOT NULL,
        year mediumint UNSIGNED NOT NULL,
        hidden boolean NOT NULL DEFAULT 1,
        PRIMARY KEY(module_id)
      )ENGINE=INNODB";

      $query[3] = "CREATE TABLE semester
      (
        semester_id smallint UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
        name varchar(100) NOT NULL UNIQUE,
        PRIMARY KEY(semester_id)
      )ENGINE=INNODB";

      $query[4] = "CREATE TABLE user
      (
        user_id mediumint UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
        username varchar(100) NOT NULL UNIQUE,
        firstname varchar(100) NOT NULL,
        lastname varchar(100) NOT NULL,
        password varchar(40) NOT NULL,
        admin boolean NOT NULL DEFAULT 0,
        PRIMARY KEY(user_id)
      )ENGINE=INNODB";

      $query[5] = "CREATE TABLE user_module
      (
        user_id mediumint UNSIGNED NOT NULL,
        module_id mediumint UNSIGNED NOT NULL,
        PRIMARY KEY(user_id, module_id)
      )ENGINE=INNODB";

      $query[6] = "CREATE TABLE settings
      (
        current_year mediumint UNSIGNED NOT NULL
      )ENGINE=INNODB";

      //Add the tables to the database
      for($i = 0; $i <= 6; $i++)
      {
        mysql_query($query[$i], $link);
        $message .= 'Adding Table ' . $i. ': ';

        if(mysql_error() == '')
        {
          $message .= 'Success<br>';
        }
        else
        {
          $message .= mysql_error();
          return $message;
        }
      }

      //***PREPARE QUERIES TO ADD FOREIGN KEYS

      $query[0] = "ALTER TABLE section Add
                   CONSTRAINT fk_section_page_id
                   FOREIGN KEY (page_id)
                   REFERENCES page(page_id)
                   ON DELETE CASCADE ON UPDATE CASCADE";

      $query[1] = "ALTER TABLE page Add
                   CONSTRAINT fk_page_module_id
                   FOREIGN KEY (module_id)
                   REFERENCES module(module_id)
                   ON DELETE CASCADE ON UPDATE CASCADE";

      $query[2] = "ALTER TABLE module Add
                   CONSTRAINT fk_module_semester_id
                   FOREIGN KEY (semester)
                   REFERENCES semester(semester_id)
                   ON UPDATE CASCADE";

      $query[3] = "ALTER TABLE user_module Add
                   CONSTRAINT fk_user_module_link_user_id
                   FOREIGN KEY (user_id)
                   REFERENCES user(user_id)
                   ON DELETE CASCADE ON UPDATE CASCADE";

      $query[4] = "ALTER TABLE user_module Add
                   CONSTRAINT fk_user_module_link_module_id
                   FOREIGN KEY (module_id)
                   REFERENCES module(module_id)
                   ON DELETE CASCADE ON UPDATE CASCADE";

      //Add the foreign keys to the database
      for($i = 0; $i <= 4; $i++)
      {
        mysql_query($query[$i], $link);
        $message .= 'Adding Foreign Key ' . $i . ': ';

        if(mysql_error() == '')
        {
          $message .= 'Success<br>';
        }
        else
        {
          $message .= mysql_error();
          return $message;
        }
      }

      //***PREPARE QUERIES TO ADD INDEXES

      $query[0] = "CREATE INDEX ix_section_page_id ON section(page_id)";
      $query[1] = "CREATE INDEX ix_page_reference ON page(reference)";
      $query[2] = "CREATE INDEX ix_module_code ON module(code)";
      $query[3] = "CREATE INDEX ix_user_username ON user(username)";

      //Add the indexes to the database
      for($i = 0; $i <= 3; $i++)
      {
        mysql_query($query[$i], $link);
        $message .= 'Adding Index ' . $i . ': ';

        if(mysql_error() == '')
        {
          $message .= 'Success<br>';
        }
        else
        {
          $message .= mysql_error();
          return $message;
        }
      }

      //***PREPARE QUERIES TO POPULATE LOOKUPS

      $query[0] = "INSERT INTO semester(semester_id,name) VALUES
                   (1,\"First Semester\"),
                   (2,\"Second Semester\"),
                   (3,\"Both Semesters\")";

      //Populate lookups
      for($i = 0; $i <= 0; $i ++)
      {
        mysql_query($query[$i], $link);
        $message .= 'Populating Lookup ' . $i . ': ';

        if(mysql_error() == '')
        {
          $message .= 'Success<br>';
        }
        else
        {
          $message .= mysql_error();
          return $message;
        }
      }

      mysql_query("INSERT INTO settings(current_year) VALUES (".date("Y").")", $link);
      $message .= 'Inserting Settings: ';

      if(mysql_error() == '')
      {
        $message .= 'Success<br>';
      }
      else
      {
        $message .= mysql_error();
        return $message;
      }

      mysql_close($link);

      $message .= '<br>';
      return $message;
    }
    else
    {
      //Connect to MySQL
      $link = mysql_connect(DBConfig::HOST,DBConfig::USER,DBConfig::PASSWORD);
      $message .= 'Connecting to MySQL: ';

      if(mysql_error() == '')
      {
        $message .= 'Success<br>';
      }
      else
      {
        $message .= mysql_error();
        return $message;
      }

      //Delete the database
      mysql_query("DROP DATABASE ".DBConfig::NAME,$link);
      $message .= 'Dropping Database: ';

      if(mysql_error() == '')
      {
        $message .= 'Success<br>';
      }
      else
      {
        $message .= mysql_error();
        return $message;
      }

      //Close the connection to the database
      mysql_close($link);
    }

    $message .= '<br>';
    return $message;
  }
}

$message = performQuery();

?>

<HTML>
  <HEAD>
    <TITLE>Database Testing Control</TITLE>
  </HEAD>
  <BODY>
    <?php echo $message; ?>
    <FORM action="database-test.php" method="POST">
      <input type="radio" name="action" value="create">Create Database
      <br>
      <input type="radio" name="action" value="delete">Delete Database
      <br>
      <input type="submit">
    </FORM>
  </BODY>
</HTML>