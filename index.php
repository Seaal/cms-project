<?php
require_once("TopBar.php");
require_once("UserDAO.php");

session_start();

$loggedIn = false;

if(isset($_SESSION["userID"]))
{
  $udao = new UserDAO();
  $user = $udao->getUserByID($_SESSION["userID"]);
  if($user)
  {
    $loggedIn = true;
  }
}

if($loggedIn)
{
  $topbar = new TopBar($user->user_id, $user->admin);
}
else
{
  $topbar = new TopBar(-1);
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once("components/head.html");?>

    <title>Index</title>

    <link href="default.css" rel="stylesheet">
  </head>

  <body>
    <?php echo $topbar;?>
    <div class="main container">
      <h1 class="page-header">Home</h1>
      <p>Welcome to the School of Computer Science's Module Webpages. Please use the navigation bar above to find your modules.</p>
    </div>
    <!-- Core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="libs/jquery-1.11.0.js"></script>
    <script src="libs/bootstrap-3.1.1-dist//js/bootstrap.min.js"></script>
  </body>
</html>