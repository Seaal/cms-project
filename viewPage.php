<?php

require_once("PageDAO.php");
require_once("UserDAO.php");
require_once("ModuleDAO.php");

require_once("TopBar.php");
require_once("SideBar.php");
require_once("ModuleNotFoundException.php");
require_once("PageNotFoundException.php");
require_once("Page.php");

if(isset($_GET["code"]) && isset($_GET["ref"]) && isset($_GET["year"]))
{
  $code = $_GET["code"];
  $ref = $_GET["ref"];
  $year = $_GET["year"];
}
else
{
  header("Location: 404.php");
}

session_start();

$loggedIn = false;

if(isset($_SESSION["userID"]))
{
  $udao = new UserDAO();
  $user = $udao->getUserByID($_SESSION["userID"]);
  if($user)
  {
    $loggedIn = true;
  }
  else
  {
    header("Location: logout.php");
  }
}

//Variables to hold the module and page information
$module;
$page;

//Pull Module we are trying to find from DB, or return an error if that module
//doesn't exist


$mdao = new ModuleDAO();

//echo $mdao->getCurrentYear();

try
{
  $module = $mdao->getModuleByCode($code, $loggedIn, $year);
}
catch(ModuleNotFoundException $e)
{
  header("Location: 404.php");
}

try
{
  $pdao = new PageDAO();
  $page = $pdao->getPageByRef($ref, $loggedIn, $module->module_id);
}
catch(PageNotFoundException $e)
{
  header("Location: 404.php");
}

$topbar;

if($loggedIn)
{
  $topbar = new TopBar($user->user_id, $user->admin);

  if(!$mdao->getUserOwnsModule($user->user_id, $module->module_id))
  {
    $loggedIn = false;
  }
}
else
{
  $topbar = new TopBar();
}

$page->setLoggedIn($loggedIn);

$sidebar = new SideBar($module->module_id, $ref, $loggedIn, $code, $year);

//Set the root folder for Responsive File Manager
$_SESSION['subfolder'] = $module->module_id;
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once("components/head.html");?>

    <title><?php echo htmlentities($module->name) ?> - <?php echo htmlentities($page->title) ?></title>

    <link href="default.css" rel="stylesheet">
  </head>

  <body>
    <?php echo $topbar;?>

    <div class="container-fluid">
      <div class="row">

        <?php echo $sidebar;?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" id="page">
          <?php echo $page->getHTML($module->name);?>
        </div>
      </div>
    </div>

    <!-- Core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="libs/jquery-1.11.0.js"></script>
    <script src="libs/bootstrap-3.1.1-dist//js/bootstrap.min.js"></script>
    <?php
    if($loggedIn)
    {
    ?>
      <script src="libs/jquery.validate.min.js"></script>
      <script src="js/validate-init.js"></script>
      <script src="js/page-validate.js"></script>
      <script src="js/page.js"></script>
      <script><?php echo $page->getJavascriptInit($module->code, $module->module_id, $year); ?></script>
    <?php } ?>
    <script src="js/sidebar.js"></script>
  </body>
</html>
