<?php
require_once("UserDAO.php");
require_once("ModuleDAO.php");
require_once("ModuleValidator.php");
require_once("FileHandler.php");

require_once("TopBar.php");
require_once("SideBar.php");

if(isset($_GET["code"]) && isset($_GET["year"]))
{
  $code = $_GET["code"];
  $year = $_GET["year"];
}
else
{
  header("Location: 404.php");
}

session_start();

$loggedIn = false;

if(isset($_SESSION["userID"]))
{
  $udao = new UserDAO();
  $user = $udao->getUserByID($_SESSION["userID"]);
  if($user)
  {
    $loggedIn = true;
  }
  else
  {
    header("Location: logout.php");
  }
}

//Variable to hold the module information
$module;

$mdao = new ModuleDAO();

try
{
  $module = $mdao->getModuleByCode($code, $loggedIn, $year);
}
catch(ModuleNotFoundException $e)
{
  header("Location: 404.php");
}

if($loggedIn)
{
  $topbar = new TopBar($user->user_id, $user->admin);

  if(!$mdao->getUserOwnsModule($user->user_id, $module->module_id))
  {
    $loggedIn = false;
  }
}
else
{
  $topbar = new TopBar();
}

$submitted = !empty($_POST);

if($submitted)
{
  $formData = $_POST;
  $formData["year"] = $year;
  $formData["oldCode"] = $code;

  $validator = new ModuleValidator();

  $result = $validator->validate($formData);

  if($result->getPassed())
  {
    $name = $_POST["name"];
    $newCode = $_POST["code"];
    $schoolYear = $_POST["schoolYear"];
    $semester = $_POST["semester"];
    $hidden = $_POST["hidden"];

    $mdao = new ModuleDAO();
    $success = $mdao->editModule($code, $name, $newCode, $schoolYear, $semester, $hidden, $year);

    if($success)
    {
      header("Location: moduleOverview.php?code=".$newCode."&year=".$year);
    }
  }
}

$sidebar = new SideBar($module->module_id, "", $loggedIn, $code, $year);

function getSemesterRadios($module, $submitted, &$result)
{
  $mdao = new ModuleDAO();
  $semesters = $mdao->getSemesters();

  $HTML = "";

  for($i=0;$i<sizeof($semesters);$i++)
  {
    $id = $semesters[$i]->semester_id;
    $name = $semesters[$i]->name;
    $HTML .= "
    <label class=\"radio-inline\">
      <input type=\"radio\" name=\"semester\" id=\"semesterRadio".$i."\" value=\"".$id."\"";

    $HTML .= $submitted ? $_POST["semester"] == $id ? " checked" : "" : $module->semester == $id ? " checked" : "";

    $HTML.= "> ".$name."
    </label>";
  }

  return $HTML;
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once("components/head.html");?>

    <title><?php echo $module->name ?> - Overview</title>

    <link href="default.css" rel="stylesheet">
  </head>

  <body>
    <?php echo $topbar;?>

    <div class="container-fluid">
      <div class="row">

        <?php echo $sidebar;?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"><?php echo htmlentities($module->name) ?><br><small>Overview</small><?php if($loggedIn) {?> <button type="submit" id="deleteModule" class="btn btn-danger pull-right">Delete Module <span class="glyphicon glyphicon-remove"></span></button></h1>
          <div id="errorMessage"></div>

          <form class="form-horizontal" id="editModuleForm" role="form" action="moduleOverview.php?code=<?php echo $code; ?>&year=<?php echo $year; ?>" method="post">
            <div class="form-group <?php echo $submitted && $result->getResult("name") !== true ? "has-error" : "" ?>">
              <label for="inputName" class="col-sm-2 control-label">Module Name</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="name" id="inputName" placeholder="Name" value="<?php echo isset($_POST["name"]) ? $_POST["name"] : $module->name; ?>" required autofocus>
                <?php echo $submitted && $result->getResult("name") !== true ? "<span class=\"help-block\">".$result->getResult("name")."</span>" : "";?>
              </div>
            </div>
            <div class="form-group <?php echo $submitted && $result->getResult("code") !== true ? "has-error" : "" ?>">
              <label for="inputCode" class="col-sm-2 control-label">Module Code</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="code" id="inputCode" placeholder="Code" value="<?php echo isset($_POST["code"]) ? $_POST["code"] : $module->code; ?>" required>
                <?php echo $submitted && $result->getResult("code") !== true ? "<span class=\"help-block\">".$result->getResult("code")."</span>" : "";?>
              </div>
            </div>
            <div class="form-group">
              <label for="yearRadio1" class="col-sm-2 control-label">Year</label>
              <div class="col-sm-5">
                <label class="radio-inline">
                  <input type="radio" name="schoolYear" id="yearRadio1" value="1" <?php echo $submitted ? $_POST["schoolYear"] == 1 ? " checked" : "" : $module->school_year == 1 ? " checked" : "";?>> 1
                </label>
                <label class="radio-inline">
                  <input type="radio" name="schoolYear" id="yearRadio2" value="2" <?php echo $submitted ? $_POST["schoolYear"] == 2 ? " checked" : "" : $module->school_year == 2 ? " checked" : "";?>> 2
                </label>
                <label class="radio-inline">
                  <input type="radio" name="schoolYear" id="yearRadio3" value="3" <?php echo $submitted ? $_POST["schoolYear"] == 3 ? " checked" : "" : $module->school_year == 3 ? " checked" : "";?>> 3
                </label>
                <label class="radio-inline">
                  <input type="radio" name="schoolYear" id="yearRadio4" value="4" <?php echo $submitted ? $_POST["schoolYear"] == 4 ? " checked" : "" : $module->school_year == 4 ? " checked" : "";?>> 4
                </label>
              </div>
            </div>
             <div class="form-group">
              <label for="semesterRadio1" class="col-sm-2 control-label">Semester</label>
              <div class="col-sm-5">
                <?php echo getSemesterRadios($module, $submitted, $result);?>
              </div>
            </div>
            <div class="form-group">
              <label for="visibilityRadio1" class="col-sm-2 control-label">Visibility</label>
              <div class="col-sm-5">
                <label class="radio-inline">
                  <input type="radio" name="hidden" id="visibilityRadio1" value="0" <?php echo $submitted ? $_POST["hidden"] == 0 ? " checked" : "" : $module->hidden == 0 ? " checked" : "";?>> Visible
                </label>
                <label class="radio-inline">
                  <input type="radio" name="hidden" id="visibilityRadio2" value="1" <?php echo $submitted ? $_POST["hidden"] == 1 ? " checked" : "" : $module->hidden == 1 ? " checked" : "";?>> Hidden
                </label>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-5">
                <button type="submit" class="btn btn-primary">Edit Module <span class="glyphicon glyphicon-pencil"></span></button>
              </div>
            </div>
          </form>
          <?php } ?>
        </div>
      </div>
    </div>

    <!-- Core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="libs/jquery-1.11.0.js"></script>
    <script src="libs/bootstrap-3.1.1-dist//js/bootstrap.min.js"></script>
    <script src="libs/jquery.validate.min.js"></script>
    <script src="js/validate-init.js"></script>
    <script src="js/module-validate.js"></script>
    <script src="js/page-validate.js"></script>
    <script src="js/sidebar.js"></script>
    <script src="js/module-overview.js"></script>
    <script>initModuleID(<?php echo $module->module_id;?>);</script>
    <script>initModuleValidation("<?php echo $code;?>",<?php echo $year;?>);</script>
  </body>
</html>