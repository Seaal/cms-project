<?php
require_once("UserDAO.php");
require_once("ModuleDAO.php");

require_once("TopBar.php");

session_start();

if(isset($_SESSION["userID"]))
{
  $udao = new UserDAO();
  $user = $udao->getUserByID($_SESSION["userID"]);
  if($user)
  {
    if(!$user->admin)
    {
      header("Location: index.php");
    }
  }
  else
  {
    header("Location: logout.php");
  }
}
else
{
  header("Location: index.php");
}

$topbar = new TopBar($user->user_id, $user->admin);

$mdao = new ModuleDAO();
$currentYear = $mdao->getCurrentYear();
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once("components/head.html");?>

    <title>Create Module</title>

    <link href="default.css" rel="stylesheet">
  </head>

  <body>
    <?php echo $topbar;?>
    <div class="main container">
      <h1 class="page-header">Admin Options</h1>
      <p>Current Year: <?php echo $currentYear;?></p>
      <button class="btn btn-info" id="rollover">Rollover Year</button>
    </div>
    <!-- Core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="libs/jquery-1.11.0.js"></script>
    <script src="libs/bootstrap-3.1.1-dist//js/bootstrap.min.js"></script>
    <script src="js/admin.js"></script>
  </body>
</html>