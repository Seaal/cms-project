<?php

class validatorResult
{
  private $passed;
  private $results;

  public function __construct()
  {
    $this->results = array();
    $this->passed = true;
  }

  public function getPassed()
  {
    return $this->passed;
  }

  public function getResult($key)
  {
    return $this->results[$key];
  }

  public function getResults()
  {
    return $this->results;
  }

  public function addResult($key, $result)
  {
    if($this->passed && $result !== true)
    {
      $this->passed = false;
    }

    $this->results[$key] = $result;
  }
}

?>